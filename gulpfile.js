let sass = require('gulp-sass');
let gulp = require('gulp');
let cleanCSS = require('gulp-clean-css');
let autoprefixer = require('gulp-autoprefixer');
let rename = require('gulp-rename');
let sourcemaps = require('gulp-sourcemaps');

//style paths
let sassFiles = './scss/**/*.scss',
    cssDest = './css/';

gulp.task('default', function(){
    gulp.src(sassFiles)
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        //.pipe(gulp.dest(cssDest))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest(cssDest))
});

gulp.task('watch', function() {
    gulp.watch(sassFiles, ['default']); 
});